<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Risk Adjustment Network</title>

<meta name="generator" content="WordPress 2.0.2" /> <!-- leave this for stats -->

<META NAME="KEYWORDS" CONTENT="Risk Adjustment Network" />
<META NAME="DESCRIPTION" CONTENT="Risk Adjustment Network" />

<link rel="stylesheet" href="http://www.stamonline.nl/blog/wp-content/themes/default/style.css" type="text/css" media="screen" />
<!--
<link rel="alternate" type="application/rss+xml" title="The European Health Care Blog RSS Feed" href="http://www.stamonline.nl/blog/feed/" />
<link rel="pingback" href="http://www.stamonline.nl/blog/xmlrpc.php" />

<link rel="shortcut icon" href="http://www.stamonline.nl/blog/wp-content/themes/default/images/favicon.ico" />
-->
<style type="text/css" media="screen">
/*	To accomodate differing install paths of WordPress, images are referred only here,
	and not in the wp-layout.css file. If you prefer to use only CSS for colors and what
	not, then go right ahead and delete the following lines, and the image files. */
		
	body { background: url("http://www.stamonline.nl/blog/wp-content/themes/default/images/kubrickbgcolor.jpg"); }	
	#page { background: url("http://www.stamonline.nl/blog/wp-content/themes/default/images/kubrickbg.jpg") repeat-y top; border: none; }
	#header { background: url("http://www.stamonline.nl/blog/wp-content/themes/default/images/kubrickheader.jpg") no-repeat bottom center; }
	#footer { background: url("http://www.stamonline.nl/blog/wp-content/themes/default/images/kubrickfooter.jpg") no-repeat bottom; border: none;}

/*	Because the template is slightly different, size-wise, with images, this needs to be set here
	If you don't want to use the template's images, you can also delete the following two lines. */
		
	#header 	{ margin: 0 !important; margin: 0 0 0 1px; padding: 1px; height: 198px; width: 758px; }
	#headerimg 	{ margin: 7px 9px 0; height: 192px; width: 740px; } 

/* 	To ease the insertion of a personal header image, I have done it in such a way,
	that you simply drop in an image called 'personalheader.jpg' into your /images/
	directory. Dimensions should be at least 760px x 200px. Anything above that will
	get cropped off of the image. */
	/*
	#headerimg { background: url('http://www.stamonline.nl/blog/wp-content/themes/default/images/personalheader.jpg') no-repeat top;}
	*/
</style>

<!--<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.stamonline.nl/blog/xmlrpc.php?rsd" />-->
</head>
<body>
<div id="page">


<div id="header">
	<div id="headerimg">
		<h1><a href="http://www.riskadjustment.net/">Risk Adjustment Network</a></h1>
		<div class="description">A collaborative of economists on the science and practice of regulated competition in health insurance</div>
	</div>
</div>
<hr />

	<div id="content" class="narrowcolumn">

	<?php
	if (!isset($p)||$p==0) {
		include("0.htm");
		}
	elseif ($p==1) {
		include("1.htm");
		}
	elseif ($p==2) {
		include("2.htm");
		}
	elseif ($p==3) {
		include("3.htm");
		}
	elseif ($p==4) {
		include("4.htm");
		}
	elseif ($p==5) {
		include("5.htm");
		}
	?>
	<center>:: <a href="index.php">Return To Homepage</a> ::</center>
	</div>
	
	<div id="sidebar">
		<img src="photos/RANlogo.JPG" width="200" height="50" border="0" hspace="0" vspace="5">
		<ul>
			<li class="pagenav"><h2>Member profiles</h2>
				<ul>
        		<li class="page_item"><a href="https://ie.linkedin.com/in/john-armstrong-ba22a31" title="">Armstrong, John (IE)</a></li>
        		<li class="page_item"><a href="https://www.unilu.ch/fakultaeten/wf/professuren/titularprof-dr-konstantin-beck/" title="">Beck, Konstantin (CH)</a></li>
        		<li class="page_item"><a href="https://sites.google.com/view/shuli-brammli-greenberg" title="">Brammli-Greenberg, Shuli (IS)</a></li>
        		<li class="page_item"><a href="http://mitarbeiter.fh-kaernten.at/florianbuchner/" title="">Buchner, Florian (DE)</a></li>
        		<li class="page_item"><a href="http://www.cpb.nl/en/staff/rudy-douven" title="">Douven, Rudy (NL)</a></li>
        		<li class="page_item"><a href="http://blogs.bu.edu/ellisrp/" title="">Ellis, Randy (US)</a></li>
        		<li class="page_item"><a href="https://en-coller.tau.ac.il/profile/glazer" title="">Glazer, Jacob (IS)</a></li>
        		<li class="page_item"><a href="https://www.linkedin.com/in/alberto-holly-71555410" title="">Holly, Alberto (CH)</a></li>
        		<li class="page_item"><a href="https://www.unilu.ch/fakultaeten/gmf/professuren/lehr-und-forschungsbeauftragte/lukas-kauer-phd/" title="">Kauer, Lukas (CH)</a></li>
        		<li class="page_item"><a href="https://hcp.hms.harvard.edu/people/timothy-j-layton" title="">Layton, Timothy (US)</a></li>
        		<li class="page_item"><a href="https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-iv/faecher/volkswirtschaftslehre/professuren/sozialpolitik/personen/prof-dr-normann-lorenz/forschung" title="">Lorenz, Normann (DE)</a></li>
        		<li class="page_item"><a href="http://www.hcp.med.harvard.edu/faculty/core/thomas-mcguire-phd" title="">McGuire, Thomas (US)</a></li>
        		<li class="page_item"><a href="https://www.newcastle.edu.au/profile/francesco-paolucci" title="">Paolucci, Francesco (AU)</a></li>
        		<li class="page_item"><a href="https://www.linkedin.com/in/lenny-pirktl-18835827" title="">Pirktl, Lenny (CH)</a></li>
        		<li class="page_item"><a href="https://profiles.stanford.edu/sherrirose" title="">Rose, Sherri (US)</a></li>
        		<li class="page_item"><a href="https://www.linkedin.com/in/sonja-schillo-8b18a810/" title="">Schillo, Sonja (DE)</a></li>
        		<li class="page_item"><a href="https://www.css.ch/en/institute/about-us/team/christian-schmid.html" title="">Schmid, Christian (CH)</a></li>
        		<li class="page_item"><a href="http://www.kuleuven.be/wieiswie/nl/person/00009398" title="">Schokkaert, Erik (BE)</a></li>
        		<li class="page_item"><a href="https://www.eur.nl/eshpm/people/erik-schut" title="">Schut, Erik (NL)</a></li>
        		<li class="page_item"><a href="https://www.israelhpr.org.il/en/organization/prof-amir-shmueli/" title="">Shmueli, Amir (IS)</a></li>
        		<li class="page_item"><a href="https://research.vu.nl/en/persons/piet-stam" title="">Stam, Piet (NL)</a></li>
        		<li class="page_item"><a href="https://www.eur.nl/eshpm/people/wynand-van-de-ven" title="">Van de Ven, Wynand (NL)</a></li>
        		<li class="page_item"><a href="https://www.eur.nl/eshpm/people/richard-van-kleef" title="">Van Kleef, Richard (NL)</a></li>
        		<li class="page_item"><a href="https://www.linkedin.com/in/suzanne-van-veen-wubben-93961119" title="">Van Veen, Suzanne (NL)</a></li>
        		<li class="page_item"><a href="https://www.eur.nl/eshpm/people/rene-van-vliet/" title="">Van Vliet, Ren� (NL)</a></li>
        		<li class="page_item"><a href="https://www.mm.wiwi.uni-due.de/team/lehrstuhlinhaber/juergen-wasem/" title="">Wasem, Juergen (DE)</a></li>
        		<li class="page_item"><a href="https://www.eur.nl/eshpm/people/anja-withagen-koster" title="">Withagen-Koster, Anja (NL)</a></li>				</ul>
				</ul>
			</li>
			<li class="pagenav"><h2>Permanent guest profile</h2>
				<ul>
        		<li class="page_item"><a href="https://www.linkedin.com/in/dr-sylvia-demme-1b5457113/" title="">Demme, Sylvia (DE)</a></li>
				</ul>
			</li>
		</ul>
	</div>



<hr />
<div id="footer">
	<p>
		This website is powered by 
		<a href="http://www.stamonline.nl">Stam Online</a>.
</p>
</div>
</div>

<!-- Gorgeous design by Michael Heilemann - http://binarybonsai.com/kubrick/ -->

		</body>
</html>
